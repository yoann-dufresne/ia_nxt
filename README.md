# Documentation du projet d'IA pour le concours de robotique de l'AFIA

## Exploration

### Génération de la visualisation de la fonction de cout de l'algo d'exploration

* Les données sont générées dans l'algo d'exploration (le fichier `dumping.txt` est créé)
* La visualisation se fait avec un script python qui va générer une image pour chaque étape et les assembler en un gif avec imagemagick

Installation des dépendances du script :

 * `sudo apt-get install imagemagick python-pip python-pip-wheel libfreetype6-dev`
 * `sudo pip install -r requirements.txt`
 * `mkdir fig`

Génerer les images :

 * lancer le mainExploration.java
 * `python plot_score_function.py`
 * regarder le fichier `disco.gif`

Voilà de quoi tout lancer : `rm -f dumping.txt fig/*.png && javac -d bin -cp src src/main/MainExploration.java && java -cp ./bin  main.MainExploration && python plot_score_function.py && eog disco.gif`