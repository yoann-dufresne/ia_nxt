package mock;

import java.awt.Point;
import java.util.List;

import model.Direction;
import model.Position;

public class Movement {
	public Position position;

	public Movement(Position position) {
		this.position = position;
	}
	
	public void followPath(List<Point> path) {
		for(Point p: path){
			if(p.equals(this.position.getPoint())){
				continue;
			}
			
			this.moveTo(p);
		}
	}
	
	// ----------- Basic movements -----------
	
	public void moveTo(Point p) {
		Direction wantedDir = null;
		int diff = 0;
		
		
		if(p.x == this.position.getPoint().x){
			diff = p.y - this.position.getPoint().y;
			if (diff > 0)
				wantedDir = Direction.EAST;
			else 
				wantedDir = Direction.WEST;
		}
		else {
			// same Y
			diff =  p.x - this.position.getPoint().x;
			if (diff > 0)
				wantedDir = Direction.SOUTH;
			else
				wantedDir = Direction.NORTH;
		}
		
		this.turn(wantedDir);
		this.forward(Math.abs(diff));
	}

	public void turn(Direction wantedDir) {
		if(wantedDir == this.position.getDirection())
			return;

		int turnValue = this.position.getDirection().turnTo(wantedDir);
		
		if(turnValue < 0) {
			this.turnLeft(-turnValue);
		} else 
			this.turnRight(turnValue);
	}

	private void turnLeft(int repeat) {
		for(int i= 0; i < repeat; i++)
			this.turnLeft();
	}

	private void turnRight(int repeat) {
		for(int i= 0; i < repeat; i++)
			this.turnRight();
	}
	
	public void forward (double distance, boolean stop) {		
	}
	
	public void forward (int nbTiles){
		switch (this.position.getDirection()) {
		case NORTH:
			this.position.updateX(-nbTiles);
			break;
		case SOUTH:
			this.position.updateX(nbTiles);
			break;
		case EAST:
			this.position.updateY(nbTiles);
			break;
		case WEST:
			this.position.updateY(-nbTiles);
			break;

		default:
			break;
		}

	}
	
	public void turnRight () {
		this.position.turnRight();
	}
	
	public void turnLeft () {
		this.position.turnLeft();
	}
	
	public void uTurn () {
		this.position.turnRight();
		this.position.turnRight();
	}
}
