package mock;


public class WallDetectors {	
	private boolean stopped;
	private boolean isInFrontPosition;

	//private boolean testSides;
	
	public WallDetectors () {
		this.isInFrontPosition = false;
		//this.testSides = false;
	}
	
	public void changeHeadPosition () {
		this.isInFrontPosition = !this.isInFrontPosition;
	}
	
	public boolean isInFrontPosition () {
		return this.isInFrontPosition;
	}

	public void stop () {
		this.stopped = true;
	}
	
	public boolean isStopped () {
		return this.stopped;
	}
}
