package ia;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import model.Direction;

public class DijkstraSolver {
	private int[][] distance;
	private Point[][] parkoor;
	private List<Direction>[][] directions;
	
	public DijkstraSolver(int x, int y){
		this.distance = new int[x][y];
		this.parkoor = new Point[x][y];
		this.directions = new ArrayList[x][y];
	}
	
	public Point[][] getParkoor(){
		return this.parkoor;
	}
	
	public int[][] getDistance(){
		return this.distance;
	}
	
	public List<Direction>[][] getDirections(){
		return this.directions;
	}
	
	public void setParkoor(Point[][] parkoor){
		this.parkoor = parkoor;
	}
	
	public void setDistance(int[][] distance){
		this.distance = distance;
	}
	
	public void setDirections(List<Direction>[][] directions){
		this.directions = directions;
	}
	
	public void setParkoorOn(int x, int y, Point value){
		this.parkoor[x][y]= value;
	}
	
	public void setDistanceOn(int x, int y, int value){
		this.distance[x][y]= value;
	}
	
	public void setDirectionsOn(int x, int y, List<Direction> value){
		this.directions[x][y]= value;
	}
	
	public void addDirectionOn(int x, int y, Direction value){
		this.directions[x][y].add(value);
	}


}
