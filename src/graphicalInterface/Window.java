package graphicalInterface;

import ia.Solution;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import model.Grid;
import model.Position;

@SuppressWarnings("serial")
public class Window extends JFrame {

	private Grid grid;
	private Position position;
	private GridPanel gridPanel;
	
	public Window (Grid grid, Position position) {
		this.grid = grid;
		this.position = position;
		this.initUI();
	}
	
    private void initUI() {    
        this.setTitle("Simple example");
        this.setSize(
        		25*(this.grid.getWidth()+2),
        		25*(this.grid.getHeight()+2)
        );

        this.setLayout(new BorderLayout());
        this.gridPanel = new GridPanel(this.grid, this.position);
        this.add(this.gridPanel);

        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
	
	public void refreshWithPause(int ms){
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.repaint();
	}

	public void setSolution(Solution sol) {
		this.gridPanel.updateSol(sol);
	}
	
}
