package graphicalInterface;
import ia.Solution;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import model.Grid;
import model.Position;
import model.Tile;
import model.WallState;

@SuppressWarnings("serial")
public class GridPanel extends JPanel{

	private int colNb;
	private int rowNb;
	private Grid grid;
	
	private Rectangle[][] cells;
	private Position position;
	private List<Point> robot1Path;
	private List<Point> robot2Path;
	private char[][] possibleDestinations;

	public GridPanel(Grid grid, Position position) {
		this.grid = grid;
		this.colNb = grid.getWidth();
		this.rowNb = grid.getHeight();
		this.cells = new Rectangle[rowNb][colNb];
		this.setSize(
        		25*this.grid.getWidth(),
        		25*this.grid.getHeight()
        );
		
		this.position = position;
		this.robot1Path = new ArrayList<Point>();
		this.robot2Path = new ArrayList<Point>();
		this.possibleDestinations = new char[0][0];
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g.create();

		int cellWidth = 25;
		int cellHeight = 25;

		for (int row = 0; row < this.rowNb; row++) {
			for (int col = 0; col < this.colNb; col++) {
				Rectangle cell = new Rectangle(
						col * cellWidth,
						row * cellHeight,
						cellWidth,
						cellHeight);
				this.cells[row][col] = cell;
			}
		}
		
		this.colorDestinations(g2d);
		this.colorRobotStartTile(g2d);
		for (int row = 0; row < this.rowNb; row++)
			for (int col = 0; col < this.colNb; col++){
				Tile tile = grid.getTile(row, col);
				Rectangle cell = this.cells[row][col];
				colorTileBorder(g2d, tile, cell);
				colorWalls(g2d, tile, cell);
		}
		
		colorPath(this.robot2Path, g2d, 1);
		colorPath(this.robot1Path, g2d, 0);
		
		g2d.dispose();
	}

	private void colorRobotStartTile(Graphics2D g2d) {
		int xRobot = new Double(Math.floor(this.position.getX())).intValue();
		int yRobot = new Double(Math.floor(this.position.getY())).intValue();
		g2d.setColor(Color.RED);
		g2d.fill(this.cells[xRobot][yRobot]);
		g2d.setColor(Color.BLACK);
	}
	
	private void colorDestinations(Graphics2D g2d) {
		for (int i=0 ; i<this.possibleDestinations.length ; i++)
			for (int j=0 ; j<this.possibleDestinations[i].length ; j++) {
				if (this.possibleDestinations[i][j] == 255)
					continue;

				g2d.setColor(Color.GRAY);
				g2d.fill(this.cells[i][j]);
				g2d.setColor(Color.BLACK);
			}
	}

	private final Color[] colors = {Color.RED, Color.GREEN};
	
	private void colorPath(List<Point> path, Graphics2D g2d, int offset) {
		g2d.setColor(this.colors[offset]);
		for (int idx=1 ; idx<path.size() ; idx++) {
			Point start = path.get(idx-1);
			Point end = path.get(idx);
			
			Rectangle cellStart = this.cells[start.x][start.y];
			Rectangle cellEnd = this.cells[end.x][end.y];
			
			int xStart = new Double(cellStart.x + 0.5 * cellStart.width).intValue() + offset;
			int yStart = new Double(cellStart.y + 0.5 * cellStart.height).intValue() + offset;
			
			int xEnd = new Double(cellEnd.x + 0.5 * cellEnd.width).intValue() + offset;
			int yEnd = new Double(cellEnd.y + 0.5 * cellEnd.height).intValue() + offset;
			
			g2d.drawLine(xStart, yStart, xEnd, yEnd);
		}
		g2d.setColor(Color.black);
	}

	
	private void colorTileBorder(Graphics2D g2d, Tile tile, Rectangle cell) {
		g2d.setColor(Color.black);
		for (int row = 0; row < this.rowNb; row++)
			for (int col = 0; col < this.colNb; col++)
				g2d.draw(this.cells[row][col]);
		g2d.setColor(Color.black);
	}
	
	private void colorWalls(Graphics2D g2d, Tile tile, Rectangle cell) {
		if (tile.east != WallState.Empty) {
			Rectangle wall = new Rectangle(
					cell.x + cell.width - 2,
					cell.y,
					2,
					cell.height
			);
			WallState currentState = tile.east;

			if (currentState == WallState.Wall) {
				g2d.setColor(Color.black);
			} else if (currentState == WallState.Undiscovered) {
				g2d.setColor(new Color(220, 200, 220));
			}
			g2d.draw(wall);
			g2d.fill(wall);
			g2d.setColor(Color.black);

		}
		if (tile.west != WallState.Empty) {
			Rectangle wall = new Rectangle(
					cell.x,
					cell.y,
					2,
					cell.height
			);
			WallState currentState = tile.west;

			if (currentState == WallState.Wall) {
				g2d.setColor(Color.black);
			} else if (currentState == WallState.Undiscovered) {
				g2d.setColor(Color.gray);
			}
			g2d.draw(wall);
			g2d.fill(wall);
			g2d.setColor(Color.black);
		}
			
		if (tile.north != WallState.Empty) {
			Rectangle wall = new Rectangle(
					cell.x,
					cell.y,
					cell.width,
					2
			);
			WallState currentState = tile.north;
			if (currentState == WallState.Wall) {
				g2d.setColor(Color.black);
			} else if (currentState == WallState.Undiscovered) {
				g2d.setColor(Color.gray);
			}
			g2d.draw(wall);
			g2d.fill(wall);
			g2d.setColor(Color.black);
		}


		if (tile.south != WallState.Empty) {
			Rectangle wall = new Rectangle(
					cell.x,
					cell.y + cell.height -2,
					cell.width,
					2
			);
			WallState currentState = tile.south;
			
			if (currentState == WallState.Wall) {
				g2d.setColor(Color.black);
			} else if (currentState == WallState.Undiscovered) {
				g2d.setColor(Color.gray);
			}
			g2d.draw(wall);
			g2d.fill(wall);
			g2d.setColor(Color.black);
		}

	}

	public void updateSol(Solution sol) {
		if (sol.alonePath != null) {
			this.robot1Path = sol.alonePath;
			this.robot2Path = new ArrayList<Point>();
		} else {
			this.robot1Path = sol.otherCoopPath;
			this.robot2Path = sol.myCoopPath;
		}
		
		this.possibleDestinations = sol.aloneParkoor;
		
		this.repaint();
	}
	
	
}
	
