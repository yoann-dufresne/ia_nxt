package main;

import ia.AbstractExplorer;
import ia.WallValuesExplorer;
import ia.ZigZagExplorer;
import graphicalInterface.Window;
import mock.Movement;
import mock.WallDetectors;
import mock.WallObserver;
import model.Direction;
import model.Grid;
import model.GridExample;
import model.Position;
import model.WallState;

public class MainExploration {

	// details des capteurs
	// S1 == A == back
	// S2 == B == front
	
	public static void main(String[] args) {
		String dumpFile = "dumping.txt";
		
		Position position = new Position(0.5, 0.5, Direction.EAST);
//		Grid trueGrid = new Grid(11, 23, WallState.Empty);

		Grid gridToExplore = new Grid(11, 23);
		Grid trueGrid = GridExample.g;
		trueGrid.addWall(0, 1, Direction.EAST);

		//Grid trueGrid = GridExample.g;
		trueGrid.serialize(dumpFile);
		
		Movement move = new Movement(position);
		WallDetectors wd = new WallDetectors();
		WallObserver wo = new WallObserver(trueGrid, move, wd);
		
		
		Window w = new Window(gridToExplore, position);
		w.setVisible(true);
	
		
		AbstractExplorer explorer = new WallValuesExplorer(position, move, wd, wo, gridToExplore, w, dumpFile);
		explorer.explore();
	}
	
}
