package main;

import graphicalInterface.Window;
import ia.IA;
import ia.Solution;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import model.Direction;
import model.Grid;
import model.GridExample;
import model.GridGenerator;
import model.Position;

public class MainIA {

	public static void main(String[] args) {
		Position position = new Position(0.5, 0.5, Direction.EAST);
//		Grid g = new Grid(11, 23, WallState.Empty);
//		g.addWall(1, 1, Direction.NORTH);
//		g.addWall(1, 1, Direction.EAST);
//		g.addWall(2, 3, Direction.NORTH);
		
//		Grid g = GridGenerator.generate(23, 11, 0.15);
		Grid g = GridExample.g;
		
		Point destination = new Point(9,  22);
		
		Window w = new Window(g, position);
		w.setVisible(true);/**/
		
		IA ia = new IA(position, g);
		Solution sol = ia.goTo(destination.x, destination.y);
		
		/*Tuple tuple = ia.parkoor(position.getPoint(), destination, destinations);
		List<Point> path2 = null;
		if (tuple.values != null && tuple.values[destination.x][destination.y] != 255) {
			Tile t = g.getTile(tuple.tileUsed.x, tuple.tileUsed.y);
			
			List<Direction> emptyWalls = new ArrayList<Direction>(4);
			if (t.east == WallState.Empty)
				emptyWalls.add(Direction.EAST);
			if (t.west == WallState.Empty)
				emptyWalls.add(Direction.WEST);
			if (t.north == WallState.Empty)
				emptyWalls.add(Direction.NORTH);
			if (t.south == WallState.Empty)
				emptyWalls.add(Direction.SOUTH);

			for (Direction dir : emptyWalls)
				g.addWall(tuple.tileUsed.x, tuple.tileUsed.y, dir);
			
			path2 = ia.traceback(tuple.values, destination);
			
			for (Direction dir : emptyWalls)
				g.removeWall(tuple.tileUsed.x, tuple.tileUsed.y, dir);
			
			path = ia.goTo(tuple.tileUsed.x, tuple.tileUsed.y).alonePath;
			
		} else
			path2 = new ArrayList<Point>();
		System.out.println(path2.size());*/
		
		w.setSolution(sol);
	}
	
}
