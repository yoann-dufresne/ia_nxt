package model;

public enum Direction {
	NORTH, EAST, SOUTH, WEST;
	
	public Direction turnLeft () {
		return Direction.values()[(Direction.values().length + this.ordinal()-1)%Direction.values().length];
	}
	
	public Direction turnRight () {
		return Direction.values()[(Direction.values().length + this.ordinal()+1)%Direction.values().length];
	}

	public int turnTo(Direction wantedDir) {
		if (Math.abs(this.ordinal() - wantedDir.ordinal()) == 2)
			return 2;
		
		if (this.ordinal() - wantedDir.ordinal() == 0)
			return 0;
		 
		if (this == NORTH) {
			if (wantedDir == WEST)
				return -1;
			else
				return 1;
		} else if (this == EAST) {
			if (wantedDir == NORTH)
				return -1;
			else
				return 1;
		} else if (this == SOUTH) {
			if (wantedDir == EAST)
				return -1;
			else
				return 1;
		} else {
			if (wantedDir == SOUTH)
				return -1;
			else
				return 1;
		}
	}
}
