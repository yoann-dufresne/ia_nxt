package model;

public class GridView {
    
    private Grid grid;
    private int x_from;
    private int x_to;
    private int y_from;
    private int y_to;

    public GridView(Grid grid, int x_from, int x_to, int y_from, int y_to) {
        this.grid = grid;
        this.x_from = x_from;
        this.x_to = x_to;
        this.y_from = y_from;
        this.y_to = y_to;
    }
    
    public Tile getTile(int x, int y) {
        return this.grid.getTile(x + this.getHeight(), y + this.getWidth());
    }
    
    public int getHeight() {
        return this.x_to - this.x_from;
    }
    
    public int getWidth() {
    	return this.y_to - this.y_from;
    }
}
