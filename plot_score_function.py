#-*- encoding:utf8 -*-
import subprocess
from multiprocessing import Pool, cpu_count

import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
from matplotlib import patches


XM, YM = (11, 23)

FILENAME = "dumping.txt"

def get_costs():
    costs = [np.zeros((XM, YM))]
    robots = [[]]
    end_walls = False
    tmp = []
    with open(FILENAME) as f:
        for line in f:
            if "--- END WALLS" in line:
                end_walls = True
                continue

            if end_walls:
                if "---" in line:
                    costs.append(np.zeros((XM, YM)))
                    robots.append([])
                else:
                    try:
                        x, y, s = [int(c) for c in line.split(",")]
                        costs[-1][x, y] = s
                    except ValueError:
                        vals = line.split(",")
                        x, y = [int(c) for c in vals[:-1]]
                        robots[-1].append((x, y, "green"))
    return costs, robots


def get_maze():
    walls = []
    with open(FILENAME) as f:
        for line in f:
            if "--- END WALLS" in line:
                break
            x, y, dir_ = line.strip().split(",")
            walls.append((int(x), int(y), dir_))
    return walls




def add_wall_south(x, y, ax, color, xm, ym):
    # ax.invert_yaxis();
    x, y = y, x
    x = x-0.05 + 1
    y = xm-y-1-0.05

    wall = patches.Rectangle((x, y), 1, 0.1, color=color, transform=ax.transData)
    ax.add_patch(wall)


def add_wall_east(x, y, ax, color, xm, ym):
    # ax.invert_yaxis();
    x, y = y, x
    x = x-0.05 + 1
    y = xm-y-1
    # import ipdb; ipdb.set_trace()

    # x = 2: y = 10 OK
    wall = patches.Rectangle((x, y), 0.1, 1, color=color, transform=ax.transData)
    ax.add_patch(wall)


def draw_maze(walls, ax, color, xm, ym):
    for x, y, orientation in walls:
        if orientation == "S":
            add_wall_south(x, y, ax, color, xm, ym)
        elif orientation == "E":
            add_wall_east(x, y, ax, color, xm, ym)


def draw_robots(robots, ax, xm, ym):
    for x, y, color in robots:
        x, y = y, x
        y = xm-y-1
        wall = patches.Rectangle((x, y), 1, 1, color=color)
        ax.add_patch(wall)


def generate_frame((cost, robots, maze, i)):
    fig = plt.figure(figsize=(7, 3))
    ax = sns.heatmap(cost, linewidths=0.1)
    ax.set_title(u"Generation n°{:0>3}".format(i))
    draw_maze(maze, ax, "black", XM, YM)
    draw_robots(robots, ax, XM, YM)
    fname = "./fig/score_{:0>3}.png".format(i)
    plt.savefig(fname)
    plt.clf()
    plt.close(fig)

    print("generated : " + fname)

def generate_frames(data):
    params = []
    for i, (cost, robots) in enumerate(zip(data["costs"], data["robots"])):
        params.append((cost, robots, data["maze"], i))

    p = Pool(cpu_count()+1)
    p.map(generate_frame, params)

def main():
    costs, robots = get_costs()
    data = {
        "costs": costs[1:],
        "robots": robots[1:],
        "maze": get_maze()
    }
    generate_frames(data)
    print("Creating the gif : disco.gif")
    subprocess.call(['convert', 'fig/*.png', 'disco.gif'])

if __name__ == '__main__':
    main()