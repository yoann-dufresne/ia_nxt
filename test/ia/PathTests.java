package ia;

import java.awt.Point;
import java.util.List;

import model.Direction;
import model.Grid;
import model.Position;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PathTests {

	private Position position;
	private Grid g;
	private IA ia;

	@Before
	public void initialize() {
		this.position = new Position(1, 1, Direction.EAST);
		this.g = new Grid(11, 23);
		g.addWall(0, 1,  Direction.SOUTH);
		g.addWall(1, 2,  Direction.EAST);
		
		this.ia = new IA(position, g);
	}
	
	@Test
	public void goToTest() {
		Solution sol = this.ia.goTo(10, 2);
		List<Point> path = sol.alonePath;
		
		System.out.println(path);
		Assert.assertTrue(path.size() == 3);
		Assert.assertTrue(path.get(0).equals(new Point(1, 1)));
		Assert.assertTrue(path.get(1).equals(new Point(1, 2)));
		Assert.assertTrue(path.get(2).equals(new Point(10, 2)));
	}
	
	/*@Test
	public void goToTest3_4 () {
		Position position = new Position(1.5, 0.5, Direction.EAST);
		Grid g = new Grid(3, 4);
		g.addWall(1, 1, Direction.NORTH);
		g.addWall(1, 1, Direction.EAST);
		g.addWall(2, 3, Direction.NORTH);
		
		IA ia = new IA(position, g);
		List<Point> path = ia.goTo(0, 2);
		System.out.println(path);
		Assert.assertTrue(path.size() == 5);
	}/**/

}
