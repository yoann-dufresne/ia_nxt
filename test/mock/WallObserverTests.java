package mock;

import model.Direction;
import model.Grid;
import model.Position;
import model.WallState;

import org.junit.Assert;
import org.junit.Test;

public class WallObserverTests {
	
	public static final int MAX_DISTANCE_SENSOR = 250;
	public static final int MAX_NB_CASE_SENSOR = 6;
	
	@Test
	public void completeDistanceTest() {
		Position position = new Position(1.5, 0.5, Direction.EAST);
		Grid trueGrid = new Grid(12, 34, WallState.Empty);
			
		trueGrid.addWall(1, 1, Direction.NORTH);
		trueGrid.addWall(2, 3, Direction.NORTH);
		
		Movement move = new Movement(position);
		WallDetectors wd = new WallDetectors();
		WallObserver wo = new WallObserver(trueGrid, move, wd);
		
		Assert.assertTrue(wo.getNbCase_B() == 1);
		Assert.assertTrue(wo.getNbCase_A() == MAX_NB_CASE_SENSOR);
		wd.changeHeadPosition();
		Assert.assertTrue(wo.getNbCase_B() == MAX_NB_CASE_SENSOR);
		Assert.assertTrue(wo.getNbCase_A() == 0);
		wd.changeHeadPosition();

		move.forward(1);

		Assert.assertTrue(wo.getNbCase_B() == 0);
		Assert.assertTrue(wo.getNbCase_A() == MAX_NB_CASE_SENSOR);
		wd.changeHeadPosition();
		Assert.assertTrue(wo.getNbCase_B() == MAX_NB_CASE_SENSOR);
		Assert.assertTrue(wo.getNbCase_A() == 1);
		wd.changeHeadPosition();
	}

	
	@Test
	public void thresholdTest() {
		Position p = new Position(50, 50, Direction.EAST);
		Grid largeGrid = new Grid(100, 100);
		Movement m = new Movement(p);

		WallDetectors detector = new WallDetectors();
		WallObserver observer = new WallObserver(largeGrid, m, detector);

		Assert.assertTrue(observer.getNbCase_B() == MAX_NB_CASE_SENSOR);
		Assert.assertTrue(observer.getNbCase_A() == MAX_NB_CASE_SENSOR);
		detector.changeHeadPosition();
		Assert.assertTrue(observer.getNbCase_B() == MAX_NB_CASE_SENSOR);
		Assert.assertTrue(observer.getNbCase_A() == MAX_NB_CASE_SENSOR);

	}
	
	
	@Test
	public void directionTest(){
		Position p = new Position(50, 50, Direction.EAST);
		Assert.assertTrue(p.getDirection() == Direction.EAST);
		p.turnLeft();
		Assert.assertTrue(p.getDirection() == Direction.NORTH);
		p.turnLeft();
		Assert.assertTrue(p.getDirection() == Direction.WEST);
		p.turnLeft();
		Assert.assertTrue(p.getDirection() == Direction.SOUTH);
		p.turnLeft();
		Assert.assertTrue(p.getDirection() == Direction.EAST);

		
		p.turnRight();
		Assert.assertTrue(p.getDirection() == Direction.SOUTH);
		p.turnRight();
		Assert.assertTrue(p.getDirection() == Direction.WEST);
		p.turnRight();
		Assert.assertTrue(p.getDirection() == Direction.NORTH);
		p.turnRight();
		Assert.assertTrue(p.getDirection() == Direction.EAST);
	}
	
	@Test
	public void headDirectionTest(){
		Position p = new Position(50, 50, Direction.EAST);
		Grid largeGrid = new Grid(100, 100);
		Movement m = new Movement(p);

		WallDetectors detector = new WallDetectors();
		WallObserver observer = new WallObserver(largeGrid, m, detector);
		boolean front = true;
		
		Assert.assertTrue(p.getDirection() == Direction.EAST);
		Assert.assertTrue(observer.getOrientation(front) == Direction.NORTH);
		Assert.assertTrue(observer.getOrientation(!front) == Direction.SOUTH);
		detector.changeHeadPosition();
		Assert.assertTrue(observer.getOrientation(front) == Direction.EAST);
		Assert.assertTrue(observer.getOrientation(!front) == Direction.WEST);
		detector.changeHeadPosition();

		p.turnLeft();
		Assert.assertTrue(p.getDirection() == Direction.NORTH);
		Assert.assertTrue(observer.getOrientation(front) == Direction.WEST);
		Assert.assertTrue(observer.getOrientation(!front) == Direction.EAST);
		detector.changeHeadPosition();
		Assert.assertTrue(observer.getOrientation(front) == Direction.NORTH);
		Assert.assertTrue(observer.getOrientation(!front) == Direction.SOUTH);
		detector.changeHeadPosition();	
		
		p.turnLeft();
		Assert.assertTrue(p.getDirection() == Direction.WEST);
		Assert.assertTrue(observer.getOrientation(front) == Direction.SOUTH);
		Assert.assertTrue(observer.getOrientation(!front) == Direction.NORTH);
		detector.changeHeadPosition();
		Assert.assertTrue(observer.getOrientation(front) == Direction.WEST);
		Assert.assertTrue(observer.getOrientation(!front) == Direction.EAST);
		detector.changeHeadPosition();	
		
		p.turnLeft();
		Assert.assertTrue(p.getDirection() == Direction.SOUTH);
		Assert.assertTrue(observer.getOrientation(front) == Direction.EAST);
		Assert.assertTrue(observer.getOrientation(!front) == Direction.WEST);
		detector.changeHeadPosition();
		Assert.assertTrue(observer.getOrientation(front) == Direction.SOUTH);
		Assert.assertTrue(observer.getOrientation(!front) == Direction.NORTH);
		detector.changeHeadPosition();	
	}

}
